//
//  LoginViewController.swift
//  VivanttEST
//
//  Created by Jiaren on 17/07/2016.
//  Copyright © 2016 Vivant. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var userIDTxt: UITextField!
    
    @IBOutlet weak var passwordTxt: UITextField!
    private var fieldAlreadyUp = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    //MARK - UITextFieldDelegate
    func textFieldDidEndEditing(textField: UITextField) {
        animateTextField(textField, up: false)
        fieldAlreadyUp = false
        textField.resignFirstResponder()
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.view.endEditing(true)
        return true
    }

    
    func textFieldDidBeginEditing(textField: UITextField) {
        if (!fieldAlreadyUp) {
            animateTextField(textField, up: true)
            fieldAlreadyUp = true
        }
    }
    
    func animateTextField(textField:UITextField, up:Bool)  {
        //CGRect keyboardBounds;
        
        let movementDistance:Int = Int(textField.frame.origin.y / 2.0);//80; // tweak as needed
        let movementDuration:Float = 0.3 // tweak as needed
        
        let movement =  (up ? -movementDistance : movementDistance);
        
        UIView.beginAnimations("anim", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(NSTimeInterval(movementDuration))
        self.view.frame = CGRectOffset(self.view.frame, 0, CGFloat(movement));
        UIView.commitAnimations()
    }
    
    @IBAction func login(sender: AnyObject) {
        if (((self.userIDTxt.text! as NSString).stringByTrimmingTrailingWhitespaceAndNewlineCharacters() as NSString).stringByTrimmingLeadingWhitespaceAndNewlineCharacters().lengthOfBytesUsingEncoding(NSUTF8StringEncoding) == 0) {
            let alert = UIAlertView(title: "Required Field", message: "Please enter User ID.", delegate: nil, cancelButtonTitle: nil, otherButtonTitles: "OK")
            alert.show()
            userIDTxt.becomeFirstResponder()
            return;
        }
        if (((self.passwordTxt.text! as NSString).stringByTrimmingTrailingWhitespaceAndNewlineCharacters() as NSString).stringByTrimmingLeadingWhitespaceAndNewlineCharacters().lengthOfBytesUsingEncoding(NSUTF8StringEncoding) == 0) {
            let alert = UIAlertView(title: "Required Field", message: "Please enter Password", delegate: nil, cancelButtonTitle: nil, otherButtonTitles: "OK")
            alert.show()
            passwordTxt.becomeFirstResponder()
            return;
        }
        
        if userIDTxt.text == kUserID && passwordTxt.text == kPassword {
            //Success login
            let defaults = NSUserDefaults.standardUserDefaults()
            defaults.setBool(true, forKey: kAlreadyLogin)
            defaults.synchronize()
            
            var vc = self.presentingViewController;
            while (vc!.presentingViewController != nil) {
                vc = vc!.presentingViewController;
            }
            vc?.dismissViewControllerAnimated(true, completion: nil)
        } else {
            //Report error
            let alert = UIAlertView(title: "Stop", message: "Either User ID or Password is wrong, please try again", delegate: nil, cancelButtonTitle: nil, otherButtonTitles: "OK")
            alert.show()
            userIDTxt.becomeFirstResponder()
            return;
        }
        
    }
}
