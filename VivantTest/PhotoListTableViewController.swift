//
//  PhotoListTableViewController.swift
//  VivanttEST
//
//  Created by Jiaren on 17/07/2016.
//  Copyright © 2016 Vivant. All rights reserved.
//

import UIKit

class PhotoListTableViewController: UITableViewController {

    var jsonItems = [PhotoDetails]()
    var imgagesDict = [String : UIImage]()   //To keep already downloaded photos
    let defaultImg = UIImage(named: "defaultImage")
    
    func loadPhotoJson()  {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        let request = NSMutableURLRequest(URL: NSURL(string:kPhotoJsonURL)!, cachePolicy: .UseProtocolCachePolicy, timeoutInterval: 60.0)
        
        request.HTTPMethod = "GET"
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue()) { (response, data, connectionError) in
            if (connectionError == nil && data != nil) {
                //As some characters couldn't be converted from data to string, need to convert to tmp variable first
                if let tmpString = NSString(data:data!, encoding:NSISOLatin1StringEncoding) {
                    let tmpData = tmpString.dataUsingEncoding(NSUTF8StringEncoding)
                    
                    var jsonDict:NSDictionary?
                    do {
                        try jsonDict = NSJSONSerialization.JSONObjectWithData(tmpData!, options: .AllowFragments) as? NSDictionary
                    } catch {
                        print("Error when retrieve JSON")
                        return
                    }
                    
                    if let photosArray = jsonDict?.objectForKey("posts") as? NSArray {
                        for dict in photosArray {
                            let details = PhotoDetails()
                            if let grapher = dict.objectForKey("photographer") as? String {
                                details.grapher = grapher
                            }
                            
                            if let caption = dict.objectForKey("caption") as? String {
                                details.caption = caption
                            }
                            if let fileName = dict.objectForKey("photo_file_name") as? String {
                                details.fileName = fileName
                            }
                            self.jsonItems.append(details)
                        }
                        //Refresh UI
                        self.tableView.reloadData()
                    }
                }
              }
        }
    
    }
   /*
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        //Check whether login or not
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.synchronize()
        
        if !defaults.boolForKey(kAlreadyLogin) {
            //Bring up intruduction and login screens
            if let intruductionVC = self.storyboard?.instantiateViewControllerWithIdentifier("introductionScreen") {
                self.presentViewController(intruductionVC, animated: true, completion: nil)
            }
         }
        
    }*/
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        //Check whether login or not
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.synchronize()
        
        if !defaults.boolForKey(kAlreadyLogin) {
            //Bring up intruduction and login screens
            if let intruductionVC = self.storyboard?.instantiateViewControllerWithIdentifier("introductionScreen") {
                self.presentViewController(intruductionVC, animated: true, completion: nil)
            }
        }
        loadPhotoJson()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let details = jsonItems[(self.tableView.indexPathForSelectedRow?.row)!]
        let photoDetailsVC = segue.destinationViewController as! PhotoDetailsViewController
        photoDetailsVC.photoDetail = details
        photoDetailsVC.chosenImgage = imgagesDict[details.fileName]
        
    }
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return jsonItems.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("photoCell", forIndexPath: indexPath) as! PhotoCell

        // Configure the cell...
        let detail = jsonItems[indexPath.row] 
        
        cell.grapher.text = detail.grapher
        cell.caption.text = detail.caption
        
        if imgagesDict[detail.fileName] == nil {
            cell.photoView.image = defaultImg
            
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)){
                self.displayingImage(indexPath)
            };
        } else {
            cell.photoView.image = imgagesDict[detail.fileName]
        }

        return cell
    }

     /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func displayingImage(indexPath:NSIndexPath)  {
        let details = jsonItems[indexPath.row]
        
        if let imageUrl = NSURL(string: String(format:"%@%@", kPhotoBaseURL, details.fileName)) {
            let imageData = NSData(contentsOfURL: imageUrl)
            if let image = UIImage(data: imageData!) {
                imgagesDict[details.fileName] = image
                
                dispatch_async(dispatch_get_main_queue()){
                
                    self.imageReceived(indexPath)
                    UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                };
            }
        }
    }
    
    func imageReceived(indexPath:NSIndexPath)  {
        let details = jsonItems[indexPath.row];
        let image = imgagesDict[details.fileName]! as UIImage
        
        let cell = self.tableView.cellForRowAtIndexPath(indexPath) as! PhotoCell
        cell.photoView.image = image;
        self.tableView.reloadRowsAtIndexPaths([indexPath],
            withRowAnimation:.None)
    }


}
