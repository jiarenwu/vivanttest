//
//  Constants.swift
//  VivantTest
//
//  Created by Jiaren on 17/07/2016.
//  Copyright © 2016 Vivant. All rights reserved.
//

import Foundation

let kPhotoJsonURL = "https://files.vivant.com.au/tech_exam/photo_posts.json"

let kPhotoBaseURL = "https://files.vivant.com.au/tech_exam/photos/"

let kUserID = "test"

let kPassword = "password"

let kAlreadyLogin = "AlreadyLogin"