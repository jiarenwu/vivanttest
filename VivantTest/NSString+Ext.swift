//
//  NSString+Ext.swift
//  Offline Charts
//
//  Created by Jiaren on 9/06/2016.
//  Copyright © 2016 Bamuda Corporation. All rights reserved.
//

import Foundation

extension NSString {
    // MARK:  Trimming Methods  (SSToolkitAdditions)
    func stringByTrimmingLeadingCharactersInSet(characterSet:NSCharacterSet) -> String {
        let rangeOfFirstWantedCharacter = self.rangeOfCharacterFromSet(characterSet.invertedSet)
        if (rangeOfFirstWantedCharacter.location == NSNotFound) {
            return "";
        }
        return self.substringFromIndex(rangeOfFirstWantedCharacter.location)
    }

    func stringByTrimmingLeadingWhitespaceAndNewlineCharacters() -> String {
        return self.stringByTrimmingLeadingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
    }
    
    func stringByTrimmingTrailingCharactersInSet(characterSet:NSCharacterSet) -> String {
        let rangeOfLastWantedCharacter = self.rangeOfCharacterFromSet(characterSet.invertedSet,
            options:.BackwardsSearch)
        if (rangeOfLastWantedCharacter.location == NSNotFound) {
            return "";
        }
        return self.substringToIndex(rangeOfLastWantedCharacter.location+1); // non-inclusive
    }
    

    func stringByTrimmingTrailingWhitespaceAndNewlineCharacters() -> String {
        return self.stringByTrimmingTrailingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet())
    }

}
