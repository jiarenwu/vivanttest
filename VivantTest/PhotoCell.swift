//
//  PhotoCell.swift
//  VivanttEST
//
//  Created by Jiaren on 17/07/2016.
//  Copyright © 2016 Vivant. All rights reserved.
//

import UIKit

class PhotoCell: UITableViewCell {

    @IBOutlet weak var photoView: UIImageView!
    
    @IBOutlet weak var caption: UITextView!
    
    @IBOutlet weak var grapher: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
