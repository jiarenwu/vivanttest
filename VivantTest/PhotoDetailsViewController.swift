//
//  PhotoDetailsViewController.swift
//  VivanttEST
//
//  Created by Jiaren on 17/07/2016.
//  Copyright © 2016 Vivant. All rights reserved.
//

import UIKit

class PhotoDetailsViewController: UIViewController {

    @IBOutlet weak var grapherLabel: UILabel!
    
    @IBOutlet weak var imageView: UIImageView!

    
    @IBOutlet weak var captionLabel: UITextView!
    
    var photoDetail:PhotoDetails? = nil
    var chosenImgage:UIImage? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        grapherLabel.text = photoDetail?.grapher
        captionLabel.text = photoDetail?.caption
        if let img = chosenImgage {
            imageView.image = img
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func favoriteTapped(sender: AnyObject) {
        UIView.animateWithDuration(5.0, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
            self.imageView.alpha = 0.0
            }, completion: {
                (finished: Bool) -> Void in
                
                  // Fade in
                UIView.animateWithDuration(5.0, delay: 1.0, options: UIViewAnimationOptions.CurveEaseIn, animations: {
                    self.imageView.alpha = 1.0
                    }, completion: nil)
        })
    }

}
